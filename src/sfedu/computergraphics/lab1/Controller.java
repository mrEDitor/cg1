package sfedu.computergraphics.lab1;

import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class Controller implements Initializable, Runnable {

    @FXML
    private Slider width, height;

    @FXML
    private ChoiceBox cleaner;

    @FXML
    private Slider angle, speed;

    @FXML
    private ToggleGroup bgColorEvent, fgColorEvent;

    @FXML
    private Slider bgColorValue, fgColorValue;

    @FXML
    private ToggleButton start;

    @FXML
    private Canvas canvas;

    @FXML
    private RadioButton bgColorHitEvent, fgColorHitEvent;

    @FXML
    private RadioButton bgColorSlowEvent, fgColorSlowEvent;

    private double[] xPoints, yPoints;

    private double[] xOldPoints, yOldPoints;

    private double xOldRect, yOldRect, wOldRect, hOldRect;

    private Thread thread;

    private GraphicsContext context;

    private Random random;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        random = new Random();
        context = canvas.getGraphicsContext2D();
        onRecolorBackground(bgColorValue.getValue());
        onRecolorForeground(fgColorValue.getValue());
        cleaner.setItems(FXCollections.observableArrayList("Очистка области (прозрачный фон)", "Перерисовка области (цветной фон)", "Перерисовка спрайта (цветной фон)"));
        cleaner.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> onCleanMethodChange(newValue.intValue()));
        cleaner.getSelectionModel().select(0);
        width.valueProperty().addListener((observable, oldValue, newValue) -> onRewidthPolygon(oldValue.doubleValue(), newValue.doubleValue()));
        height.valueProperty().addListener((observable, oldValue, newValue) -> onReheightPolygon(oldValue.doubleValue(), newValue.doubleValue()));
        bgColorValue.valueProperty().addListener((observable, oldValue, newValue) -> onRecolorBackground(newValue.doubleValue()));
        fgColorValue.valueProperty().addListener((observable, oldValue, newValue) -> onRecolorForeground(newValue.doubleValue()));
        start.selectedProperty().addListener((observable, oldValue, newValue) -> onStartStop(newValue));
        initPolygon();
        repaint();
    }

    private void onRecolorForeground(double newValue) {
        context.setStroke(Color.hsb(newValue, 1, 1, 1));
        repaint();
    }

    private void onRecolorBackground(double newValue) {
        context.setFill(Color.hsb(newValue, 0.3, 0.2, 1));
        clean();
    }

    private void onRewidthPolygon(double oldValue, double newValue) {
        double delta = (newValue - oldValue) / 2;
        xPoints[1] = Math.round(xPoints[1] - delta);
        xPoints[3] = Math.round(xPoints[3] + delta);
        clean();
    }

    private void onReheightPolygon(double oldValue, double newValue) {
        double delta = (newValue - oldValue) / 2;
        yPoints[0] = Math.round(yPoints[0] - delta);
        yPoints[2] = Math.round(yPoints[2] + delta);
        clean();
    }

    private void clean() {
        onCleanMethodChange(cleaner.getSelectionModel().getSelectedIndex());
    }

    private void onCleanMethodChange(int newMethod) {
        switch (newMethod) {
            case 0: // clear rect
                context.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
                break;
            case 1: // fill rect
            case 2: // stroke polygon
                context.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
                break;
        }
        repaint();
    }

    private void onStartStop(boolean newValue) {
        if (newValue) {
            thread = new Thread(this);
            thread.start();
        } else {
            thread = null;
        }
    }

    private void initPolygon() {
        final double w = canvas.getWidth();
        final double h = canvas.getHeight();
        xPoints = new double[]{
                Math.round(w / 2), Math.round((w - width.getValue()) / 2),
                Math.round(w / 2), Math.round((w + width.getValue()) / 2)
        };
        yPoints = new double[]{
                Math.round((h - height.getValue()) / 2), Math.round(h / 2),
                Math.round((h + height.getValue()) / 2), Math.round(h / 2)
        };
        xOldPoints = new double[4];
        yOldPoints = new double[4];
    }

    public void repaint() {
        switch (cleaner.getSelectionModel().getSelectedIndex()) {
            case 0: // clear rect
                context.clearRect(xOldRect, yOldRect, wOldRect, hOldRect);
                break;
            case 1: // fill rect
                context.fillRect(xOldRect, yOldRect, wOldRect, hOldRect);
                break;
            case 2: // stroke polygon
                context.setLineWidth(3.0);
                context.setStroke(Color.hsb(bgColorValue.getValue(), 0.3, 0.2, 1.0));
                context.strokePolygon(xOldPoints, yOldPoints, 4);
                context.setLineWidth(1.0);
                context.setStroke(Color.hsb(fgColorValue.getValue(), 1, 1, 1.0));
                break;
        }
        context.strokePolygon(xPoints, yPoints, 4);
    }

    @Override
    public void run() {
        while (thread != null) {
            try {
                fillOldValues();
                for (int i = 0; i < 4; i++) {
                    xPoints[i] = Math.round(xPoints[i] + 0.1 * speed.getValue() * Math.cos(Math.toRadians(angle.getValue())));
                    yPoints[i] = Math.round(yPoints[i] - 0.1 * speed.getValue() * Math.sin(Math.toRadians(angle.getValue())));
                }
                double xCorrection = 0;
                double yCorrection = 0;
                if (xPoints[1] < 0) {
                    xCorrection = -xPoints[1];
                } else if (xPoints[3] > canvas.getWidth()) {
                    xCorrection = canvas.getWidth() - xPoints[3];
                }
                if (yPoints[0] < 0) {
                    yCorrection = -yPoints[0];
                } else if (yPoints[2] > canvas.getHeight()) {
                    yCorrection = canvas.getHeight() - yPoints[2];
                }
                if (xCorrection != 0) {
                    for (int i = 0; i < 4; i++) {
                        xPoints[i] = Math.round(xPoints[i] + xCorrection);
                    }
                    angle.valueProperty().setValue((540 - angle.getValue()) % 360);
                }
                if (yCorrection != 0) {
                    for (int i = 0; i < 4; i++) {
                        yPoints[i] = Math.round(yPoints[i] + yCorrection);
                    }
                    angle.valueProperty().setValue((360 - angle.getValue()) % 360);
                }
                if (xCorrection != 0 || yCorrection != 0) {
                    if (bgColorHitEvent.isSelected()) {
                        bgColorValue.setValue(random.nextDouble() * 360);
                    }
                    if (fgColorHitEvent.isSelected()) {
                        fgColorValue.setValue(random.nextDouble() * 360);
                    }
                } else {
                    if (bgColorSlowEvent.isSelected()) {
                        bgColorValue.setValue((bgColorValue.getValue() + 0.05 * speed.getValue()) % 360);
                    }
                    if (fgColorSlowEvent.isSelected()) {
                        fgColorValue.setValue((fgColorValue.getValue() + 0.05 * speed.getValue()) % 360);
                    }
                }
                Platform.runLater(this::repaint);
                Thread.sleep(50);
            } catch (final InterruptedException e) {
            }
        }
    }

    private void fillOldValues() {
        System.arraycopy(xPoints, 0, xOldPoints, 0, 4);
        System.arraycopy(yPoints, 0, yOldPoints, 0, 4);
        xOldRect = xPoints[1] - 1;
        yOldRect = yPoints[0] - 1;
        wOldRect = Math.round(xPoints[3] - xPoints[1] + 2);
        hOldRect = Math.round(yPoints[2] - yPoints[0] + 2);
    }

    public void onStop() {
        thread = null;
    }
}
