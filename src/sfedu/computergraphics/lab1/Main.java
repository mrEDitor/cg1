package sfedu.computergraphics.lab1;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    private Controller controller;

    @Override
    public void start(final Stage primaryStage) throws Exception {
        final FXMLLoader loader = new FXMLLoader(getClass().getResource("main.fxml"));
        final Parent root = loader.load();
        final Scene scene = new Scene(root);
        controller = loader.getController();
        primaryStage.setTitle("CG-1");
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        controller.onStop();
        super.stop();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
